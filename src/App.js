import React,{Component} from 'react'
import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer } from 'react-navigation'
import Loading from './Loading'
import SignUp from './SignUp'
import Login from './Login'
import Main from './Main'

const AppNavigator=createStackNavigator(
    {
      Loading:Loading,
      SignUp:SignUp,
      Login:Login,
      Main:Main
    },
    {
        initialRouteName:'Login',
        defaultNavigationOptions:{
            headerTitleAlign:'center'
        }
    }
);

const AppContainer=createAppContainer(AppNavigator);
export default class App extends React.Component{
    render(){
        return <AppContainer/>;
    }
}
